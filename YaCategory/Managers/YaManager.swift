//
//  YaManager.swift
//  YaCategory
//
//  Created by Hadevs on 05/02/2018.
//  Copyright © 2018 Hadevs. All rights reserved.
//

import UIKit
import Locksmith
import YandexMoneySDKObjc
import RealmSwift

class YaManager {
    
    private let clientId = "85016761B38E00A2E1AEF374DDD0506CE6CE7B7E84BC76434CCB5641C1F19313"
    private let redirectUrlString: String = "http://ya.ru"
    private var storage: StorageManager
    private var session = YMAAPISession()
    
    init(storage: StorageManager) {
        self.storage = storage
    }
    
    var url: URL {
        return URL(string: "https://money.yandex.ru/api/categories-list")!
    }
    
    func save(token: String) throws {
        if let token = retrieveToken() {
            try Locksmith.updateData(data: ["token": token], forUserAccount: "default")
        } else {
            try Locksmith.saveData(data: ["token": token], forUserAccount: "default")
        }
    }
    
    func removeToken() throws {
        try Locksmith.deleteDataForUserAccount(userAccount: "default")
    }
    
    func requestLogin(in webView: UIWebView) {
        let request = session.authorizationRequest(withClientId: clientId, additionalParameters: [
            YMAParameterResponseType: YMAValueParameterResponseType,
            YMAParameterRedirectUri: redirectUrlString,
            YMAParameterScope: "account-info"
            ])
        webView.isHidden = false
        webView.loadRequest(request)
        webView.backgroundColor = .green
    }
    
    func retrieveToken() -> String? {
        return Locksmith.loadDataForUserAccount(userAccount: "default")?["token"] as? String
    }

    func getToken(with authCode: String, with completion: @escaping (_ error: Error?, _ token: String?) -> Void) {
        session.receiveToken(withCode: authCode, clientId: self.clientId, additionalParameters: ["grant_type": "authorization_code", YMAParameterRedirectUri: redirectUrlString], completion: { (id, error) in
            if let id = id {
                do {
                    try self.save(token: id)
                } catch {
                    completion(error, id)
                }
            }
            
            completion(error, id)
        })
    }
    
    func isRequest(request: URLRequest, authorizationInfo: AutoreleasingUnsafeMutablePointer<NSMutableDictionary?>) throws {
        try session.isRequest(request, toRedirectUrl: redirectUrlString, authorizationInfo: authorizationInfo)
    }
    
    func getAccountInfo(with token: String?, and completion: @escaping (String?, YMAAccountInfoModel?) -> Void) {
        self.session.perform(YMAAccountInfoRequest(), token: token, completion: { (request, response, error) in
            guard let response = response else {
                DispatchQueue.main.async {
                    completion("Error response", nil)
                }
                return
            }
            
            if response.statusCode == .statusCodeInvalidTokenHTTP {
                completion("Error response", nil)
            } else if response.statusCode.rawValue == 0 {
                DispatchQueue.main.async {
                    if let error = error {
                        completion(error.localizedDescription, nil)
                    } else {
                        if let response = response as? YMAAccountInfoResponse, let info = response.accountInfo {
                            completion(nil, info)
                        } else {
                            completion("Проблема с получением информации об аккаунте.", nil)
                        }
                    }
                }
            }
        })
    }
    
    func refreshCategories(with completion: @escaping ([Category]) -> Void = {_ in}) {
        
        let request: URLRequest = URLRequest.init(url: url)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            func endFunction(_ result: [Category] = []) {
                DispatchQueue.main.async {
                    completion(result)
                }
            }
            
            guard let data = data, error == nil else {
                endFunction()
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
                    self.storage.clearCategories()
                    let categories = List<Category>()
                    json.forEach({ (dict) in
                        let category = Category(json: dict)!
                        categories.append(category)
                        self.storage.save(category)
                    })
                    
                    endFunction(categories.toArray())
                } else {
                    endFunction()
                }
            } catch {
                endFunction()
                return
            }
        }
        
        task.resume()
    }
    
    func getCategories(with completion: @escaping ([Category]) -> Void = {_ in}) {
        if !(storage.categories?.toArray() ?? []).isEmpty {
            completion(storage.categories!.toArray())
        } else {
            refreshCategories(with: completion)
        }
    }
}
