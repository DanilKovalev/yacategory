//
//  StorageManager.swift
//  YaCategory
//
//  Created by Hadevs on 13/02/2018.
//  Copyright © 2018 Hadevs. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class StorageManager {
    var categories: Results<Category>? {
        let realm = try? Realm()
        let categoryObjects = realm?.objects(Category.self).filter("parrentCategoryId != %@", "")
        return categoryObjects
    }
    
    func clearCategories() {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(realm.objects(Category.self))
        }
    }
    
    func save<T:Object>(_ realmObject:T) {
        DispatchQueue.main.async {
            let realm = try! Realm()
            try! realm.write {
                realm.add(realmObject)
            }
        }
    }
}
