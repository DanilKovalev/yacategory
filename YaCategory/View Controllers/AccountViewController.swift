//
//  AccountViewController.swift
//  YaCategory
//
//  Created by Hadevs on 20/02/2018.
//  Copyright © 2018 Hadevs. All rights reserved.
//

import UIKit
import YandexMoneySDKObjc

class AccountViewController: UIViewController {

    var yaManager: YaManager?
    
    private let standartFont = UIFont(name: "Helvetica-Neue", size: 14.0)
    
    private lazy var scrollView: UIScrollView = {
        let scrollView: UIScrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    private lazy var labelsView: UIView = {
        let stackView = UIView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var avatarView: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = UIColor(netHex: 0xf1f1f1)
        imageView.layer.borderColor = UIColor.black.cgColor
        imageView.layer.masksToBounds = true
        imageView.layer.borderWidth = 1
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    
    private lazy var loginLabel: UILabel = {
        let label = defaultLabel()
        return label
    }()
    
    private lazy var numberLabel: UILabel = {
        let label = defaultLabel()
        return label
    }()
    
    private lazy var balanceLabel: UILabel = {
        let label = defaultLabel()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var currencyLabel: UILabel = {
        let label = defaultLabel()
        label.textAlignment = .right
        label.font = UIFont.boldSystemFont(ofSize: 14.0)
        return label
    }()
    
    @objc private lazy var exitButton: UIButton = {
        let button = UIButton()
        button.isEnabled = true
        button.isUserInteractionEnabled = true
        button.backgroundColor = UIColor.yandexColor
        button.setTitle("ВЫХОД", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = standartFont
        button.layer.cornerRadius = 5
        return button
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavigationBar()
        
        addViews()
        addConstraints()
        view.backgroundColor = .white
        hideInterface()
        addTargets()
        
        loadAccountInfo(with: yaManager?.retrieveToken(), error: nil)
    }
    
    private func hideInterface() {
        avatarView.isHidden = true
        loginLabel.isHidden = true
        numberLabel.isHidden = true
        scrollView.isHidden = true
        currencyLabel.isHidden = true
        balanceLabel.isHidden = true
        exitButton.isHidden = true
    }
    
    private func showInterface() {
        scrollView.isHidden = false
        avatarView.isHidden = false
        loginLabel.isHidden = false
        numberLabel.isHidden = false
        currencyLabel.isHidden = false
        balanceLabel.isHidden = false
        exitButton.isHidden = false
    }
    
    private func fillData(with info: YMAAccountInfoModel) {
        self.showInterface()
        loginLabel.text = info.account
        numberLabel.text = "Количество карт: \(info.yamoneyCards?.count ?? 0)"
        balanceLabel.text = info.balance
        currencyLabel.text = info.currency
        avatarView.downloadFrom(url: info.avatar?.url)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        avatarView.layer.cornerRadius = avatarView.frame.height / 2
    }
    
    private func defaultLabel() -> UILabel {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = standartFont
        return label
    }
    
    private func addConstraints() {
        let viewsDict: [String : Any] =
            ["avatarView": avatarView,
             "loginLabel": loginLabel,
             "numberLabel": numberLabel,
             "balanceLabel": balanceLabel,
             "currencyLabel": currencyLabel,
             "exitButton": exitButton,
             "stackView": labelsView
            ]
        
        let scrollViewContraints: [NSLayoutConstraint] = {
            return NSLayoutConstraint.constraints(withVisualFormat: "V:|[scrollView]|", options: [], metrics: nil, views: ["scrollView": scrollView]) + NSLayoutConstraint.constraints(withVisualFormat: "H:|[scrollView]|", options: [], metrics: nil, views: ["scrollView": scrollView])
        }()
        
        var constraints: [NSLayoutConstraint] = []
        
        let verticalAvatarAndLoginViewContraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-16-[avatarView(88)]-16-[loginLabel]-8-[numberLabel]-16-[stackView]-(>=16)-[exitButton(44)]-16-|", options: [], metrics: nil, views: viewsDict)
        let horizontalAvatarViewContraints = NSLayoutConstraint.constraints(withVisualFormat: "H:[avatarView(88)]", options: [], metrics: nil, views: viewsDict)
        avatarView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        constraints += verticalAvatarAndLoginViewContraints
        constraints += horizontalAvatarViewContraints
        
        let horizontalLoginLabelContraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[loginLabel]-16-|", options: [], metrics: nil, views: viewsDict)
        constraints += horizontalLoginLabelContraints
        
        let horizontalNumberLabelContraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[numberLabel]-16-|", options: [], metrics: nil, views: viewsDict)
        constraints += horizontalNumberLabelContraints
        
        let horizontalStackViewConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[stackView]-16-|", options: [], metrics: nil, views: viewsDict)
        constraints += horizontalStackViewConstraints
        
        let horizontalExitButtonContraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[exitButton]-16-|", options: [], metrics: nil, views: viewsDict)
        constraints += horizontalExitButtonContraints
        
        view.addConstraints(scrollViewContraints)
        
        labelsView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        labelsView.addSubview(balanceLabel)
        labelsView.addSubview(currencyLabel)
        
        let labelsContraints = {
            return NSLayoutConstraint.constraints(withVisualFormat: "H:|[l1]-16-[l2]|", options: [], metrics: nil, views: ["l1": balanceLabel, "l2": currencyLabel]) + NSLayoutConstraint.constraints(withVisualFormat: "V:|[l1]|", options: [], metrics: nil, views: ["l1": balanceLabel, "l2": currencyLabel]) + NSLayoutConstraint.constraints(withVisualFormat: "V:|[l2]|", options: [], metrics: nil, views: ["l1": balanceLabel, "l2": currencyLabel])
        }()
        
        labelsView.addConstraints(labelsContraints)
        scrollView.addConstraints(constraints)
    }
    
    private func addViews() {
        
        view.addSubview(scrollView)
        scrollView.addSubview(avatarView)
        scrollView.addSubview(loginLabel)
        scrollView.addSubview(numberLabel)
        scrollView.addSubview(labelsView)
        scrollView.addSubview(exitButton)
    }
    
    private func addTargets() {
        exitButton.addTarget(self, action: #selector(exitButtonClicked(sender:)), for: .touchUpInside)
    }
    
    @objc private func exitButtonClicked(sender: UIButton) {
        do {
            try yaManager?.removeToken()
            hideInterface()
            startLoggining()
        } catch {
            self.showAlert(title: "Ошибка", message: "Возникла проблемы при выходе. Попробуйте еще раз.")
        }
    }
    
    
    private func startLoggining() {
        let vc = WebAuthorizeViewController()
        vc.yaManager = yaManager
        vc.delegate = self
        present(UINavigationController.init(rootViewController: vc), animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func loadAccountInfo(with id: String?, error: Error?) {
        DispatchQueue.main.async {
            if let id = id, !id.isEmpty {
                self.yaManager?.getAccountInfo(with: id, and: { (errorString, model) in
                    if let string = errorString {
                        self.startLoggining()
                        self.showAlert(title: "Ошибка", message: string)
                    } else if let model = model {
                        self.fillData(with: model)
                    } else {
                        self.showAlert(title: "Ошибка", message: "Неизвестная ошибка")
                    }
                })
            } else {
                self.startLoggining()
            }
        }
    }
}

extension AccountViewController: WebAuthorizeViewControllerDelegate {
    func webAuthrorizeViewDidFinishLoading(with id: String?, error: Error?) {
        self.loadAccountInfo(with: id, error: error)
    }
}


extension UIImageView {
    func downloadFrom(url: URL?) {
        guard let url = url else {
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
}
