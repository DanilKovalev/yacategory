//
//  ViewController.swift
//  YaCategory
//
//  Created by Hadevs on 05/02/2018.
//  Copyright © 2018 Hadevs. All rights reserved.
//

import UIKit

class CategoriesViewController: UIViewController {

    lazy private var tableView: UITableView = {
        let tableView = UITableView()
        return tableView
    }()
    
    private var cellIdentifier: String {
        return "cell"
    }
    
    var yaManager: YaManager?
    var storageManager: StorageManager?
    
    private lazy var refreshControl: UIRefreshControl = UIRefreshControl()
    var categories: [Category] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    var isNeedToLoadMainCategories = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createTableView()
        initRefreshControl()
        loadData()
        configureNavigationBar()
        
    }
    
    private func loadData() {
        if isNeedToLoadMainCategories {
            yaManager?.getCategories {
                categories in
                self.categories = categories
            }
        }
    }
    
    private func initRefreshControl() {
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refreshControlDragged), for: UIControlEvents.valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    @objc private func refreshControlDragged() {
        yaManager?.refreshCategories { categories in
            self.categories = categories
            self.refreshControl.endRefreshing()
        }
    }
    
    
    private func createTableView() {
        tableView.frame = view.frame
        tableView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorInset = UIEdgeInsets.init(top: 0, left: 15000, bottom: 0, right: 0)
        view.addSubview(tableView)
        
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension CategoriesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let category = categories[indexPath.row]
        
        if !category.subs.isEmpty {
            let vc = CategoriesViewController()
            vc.categories = category.subs.toArray()
            vc.yaManager = yaManager
            vc.storageManager = storageManager
            
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        cell.textLabel?.text = categories[indexPath.row].title
        cell.separatorInset = .zero
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
}
