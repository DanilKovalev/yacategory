//
//  WebAuthorizeViewController.swift
//  YaCategory
//
//  Created by Hadevs on 05/03/2018.
//  Copyright © 2018 Hadevs. All rights reserved.
//

import UIKit

protocol WebAuthorizeViewControllerDelegate {
    func webAuthrorizeViewDidFinishLoading(with id: String?, error: Error?)
}

class WebAuthorizeViewController: UIViewController {
    
    private lazy var webView: UIWebView = {
        let webView = UIWebView(frame: view.bounds)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        webView.delegate = self
        return webView
    }()
    
    var yaManager: YaManager?
    var delegate: WebAuthorizeViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavigationBar()
        view.addSubview(webView)
        title = "Авторизация"
        startLoggining()
        
        // Do any additional setup after loading the view.
    }

    private func startLoggining() {
        yaManager?.requestLogin(in: webView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WebAuthorizeViewController: UIWebViewDelegate {
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        var authorizationInfo: NSMutableDictionary? = nil
        
        do {
            try yaManager?.isRequest(request: request, authorizationInfo: &authorizationInfo)
            webView.isHidden = true
            
            if let authCode = authorizationInfo?["code"] as? String {
                self.yaManager?.getToken(with: authCode, with: { (error, token) in
                    self.delegate?.webAuthrorizeViewDidFinishLoading(with: token, error: error)
                    self.dismiss(animated: true, completion: nil)
                })
            } else {
                self.showAlert(title: "Ошибка", message: "Проблема с получением кода авторизации.")
            }
            
            return false
        } catch {
            return true
        }
    }
    
    
}
