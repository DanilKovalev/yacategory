//
//  Category.swift
//  YaCategory
//
//  Created by Hadevs on 05/02/2018.
//  Copyright © 2018 Hadevs. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

class Category: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var parrentCategoryId: String = ""
    
    var subs:List<Category> = List<Category>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init?(json: [String: Any]) {
        self.init()
        guard let title = json["title"] as? String else {
                return nil
        }
        
        self.title = title
        
        if let subs = json["subs"] as? [[String: Any]] {
            for sub in subs {
                if let subCategory = Category(json: sub) {
                    subCategory.parrentCategoryId = id
                    self.subs.append(subCategory)
                }
            }
        }
        
        self.id = ID()
    }
}
