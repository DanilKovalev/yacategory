//
//  Constants.swift
//  YaCategory
//
//  Created by Hadevs on 13/02/2018.
//  Copyright © 2018 Hadevs. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

extension List {
    func toArray() -> [Element] {
        return Array(self)
    }
}

extension Results {
    func toArray() -> [Element] {
        return Array(self)
    }
}

extension UIColor {
    static var yandexColor: UIColor {
        return UIColor.init(netHex: 0xFFCC00)
    }
}

extension UIColor {
    //UIColor from HEX
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}

extension UIViewController {
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Ок", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func configureNavigationBar() {
        guard let bar = navigationController?.navigationBar else {
            return
        }
        
        bar.isTranslucent = false
        bar.barTintColor = UIColor.yandexColor
        bar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.init(name: "YandexSansDisplay-Regular", size: 18.0)!]
        bar.tintColor = .white
        title = tabBarItem.title
    }
}

