//
//  Help Functions.swift
//  YaCategory
//
//  Created by Hadevs on 13/03/2018.
//  Copyright © 2018 Hadevs. All rights reserved.
//

import Foundation

func ID() -> String {
    let time = String(Int(NSDate().timeIntervalSince1970), radix: 16, uppercase: false)
    let machine = String(arc4random_uniform(900000) + 100000)
    let pid = String(arc4random_uniform(9000) + 1000)
    let counter = String(arc4random_uniform(900000) + 100000)
    return time + machine + pid + counter
}
